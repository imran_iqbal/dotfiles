extern crate libc;

use libc::sysinfo;

const LINUX_SYSINFO_LOADS_SCALE: f64 = (1 << libc::SI_LOAD_SHIFT) as f64;

fn main() {
    let mut sysinfo_data = sysinfo {
        uptime: 0,
        loads: [0; 3],
        totalram: 0,
        freeram: 0,
        sharedram: 0,
        bufferram: 0,
        totalswap: 0,
        freeswap: 0,
        procs: 0,
        pad: 0,
        totalhigh: 0,
        freehigh: 0,
        mem_unit: 0,
        _f: [0; 0],
    };

    unsafe {
        sysinfo(&mut sysinfo_data);
    };

    println!(
        "{:.2} {:.2} {:.2}",
        sysinfo_data.loads[0] as f64 / LINUX_SYSINFO_LOADS_SCALE,
        sysinfo_data.loads[1] as f64 / LINUX_SYSINFO_LOADS_SCALE,
        sysinfo_data.loads[2] as f64 / LINUX_SYSINFO_LOADS_SCALE,
    );
}
