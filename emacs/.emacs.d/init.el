;; Please don't load outdated byte code
(setq load-prefer-newer t)

(require 'package)
(setq package-enable-at-startup nil)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "org"  (concat proto "://orgmode.org/elpa/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 2)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; set language to utf-8
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

(global-display-line-numbers-mode) ;; show line numbers

(set-default 'truncate-lines t) ;; don't wrap lines
(set-default 'indent-tabs-mode nil) ;; insert spaces instead of tabs

(show-paren-mode 1) ; highlight matching brackets

(add-to-list 'default-frame-alist '(font . "PragmataPro Mono-14")) ; set font

(setq make-backup-files nil) ; stop making backup files
(setq auto-save-default nil) ; stop making autosave files
(setq create-lockfiles nil) ; stop making lock files
(setq-default require-final-newline t) ; add newline at end of file
(setq-default show-trailing-whitespace t) ; highlight extra whitespace
(global-auto-revert-mode 1) ; auto update file if updated outside of emacs
(add-hook 'before-save-hook 'delete-trailing-whitespace) ; auto delete whitespace on save

;; disable beeping and start up screen
(setq ring-bell-function #'ignore
      inhibit-startup-screen t
      echo-keystrokes 0.1)

(fset 'yes-or-no-p #'y-or-n-p) ;; short yes or no prompts
;; Opt out from the startup message in the echo area by simply disabling this
;; ridiculously bizarre thing entirely.
(fset 'display-startup-echo-area-message #'ignore)


;; disable gui stuff
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

(use-package diminish :ensure t)

(use-package evil
  :ensure t
  :diminish undo-tree-mode
  :config
  (evil-mode 1))

(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

(use-package evil-commentary
  :ensure t
  :diminish evil-commentary-mode
  :config
  (evil-commentary-mode))

(use-package zenburn-theme
  :ensure t
  :config
  (load-theme 'zenburn t))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode t))

(use-package elixir-mode
  :ensure t
  :bind (:map elixir-mode-map
	      ("C-c C-f" . elixir-format)))

(use-package telephone-line
  :ensure t
  :init
  (setq telephone-line-lhs
        '((evil   . (telephone-line-evil-tag-segment))
          (accent . (telephone-line-vc-segment
                     telephone-line-erc-modified-channels-segment
                     telephone-line-process-segment))
          (nil    . (telephone-line-minor-mode-segment
                     telephone-line-buffer-segment))))
  (setq telephone-line-rhs
        '((nil    . (telephone-line-misc-info-segment))
          (accent . (telephone-line-major-mode-segment))
          (evil   . (telephone-line-airline-position-segment))))
  :config
  (telephone-line-mode 1)
)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(diminish telephone-line elixir-mode zenburn evil-commentary evil-surround use-package org-plus-contrib magit helm evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
