" OS detection {{{
if !exists("g:os")
    if has("win64") || has("win32") || has("win16")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
    endif
endif
" }}}

" Python binary location {{{
if g:os == "Darwin"
    let g:python_host_prog = '/usr/local/opt/python2/bin/python2'
    let g:python3_host_prog = '/usr/local/bin/python3'
elseif g:os == "Linux"
    let g:loaded_python_provider = 0
    let g:python3_host_prog = '/usr/bin/python'
endif
" }}}

" General settings {{{
let mapleader=" "  " Space bar for leader

set clipboard+=unnamedplus " Use system clipboard
syntax on
set number
set noshowmode
set nowrap
set foldmethod=marker
set colorcolumn=80
set synmaxcol=200

set listchars=tab:<->,trail:·,space:·,eol:¬
set list " Show tabs, space and nbsp

" Whitespace behaviour
set expandtab        " expand tabs by default
set softtabstop=4    " number of spaces a <Tab> character equals (insert mode)
set shiftwidth=4     " number of spaces to use for indenting
set copyindent       " copy indent structure when making new lines
set cino=(0

set nobackup
set noswapfile

set hidden

set ignorecase
set smartcase

" True color support
set termguicolors

" Use ripgrep for grep command
set grepprg=rg\ --vimgrep

" typing a semi-colon starts command (normal mode)
nnoremap ; :

" Remove highlight from searches (normal mode)
nmap <silent> <Leader>/ :nohlsearch<CR>

set background=dark

set completeopt-=preview
set shortmess+=c

augroup AutoClosePopupMenu
    autocmd!
    autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
augroup END

" Use tab + shift tab to cycle through pop up options
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <expr><S-Tab> pumvisible() ? "\<c-p>" : "\<S-Tab>"

" This happens all too often
iabbrev recieve receive
" }}}

" Plugins {{{
" Plug {{{
" Install vim-plug if we don't already have it
let plug_install = 0
let autoload_plug_path = stdpath('config') . '/autoload/plug.vim'

if !filereadable(autoload_plug_path)
    silent exe '!curl -fL --create-dirs -o ' . autoload_plug_path .
        \ ' https://raw.github.com/junegunn/vim-plug/master/plug.vim'
    execute 'source ' . fnameescape(autoload_plug_path)
    let plug_install = 1
endif

unlet autoload_plug_path

let plugged_data_path = stdpath('data') . '/plugged'
call plug#begin(plugged_data_path)
" Syntax
Plug 'posva/vim-vue', { 'for' : 'vue' }
Plug 'vmchale/dhall-vim', { 'for': 'dhall' }

" Git stuff in the gutter, undo/stage changes
Plug 'mhinz/vim-signify'

" Indent level movment commands
Plug 'jeetsukumaran/vim-indentwise'

" Aysnc linting
Plug 'dense-analysis/ale'

" Go to definition for python
Plug 'davidhalter/jedi-vim', { 'for': 'python' }

" Repeat plugin commands with .
Plug 'tpope/vim-repeat'

" Resize non active windows
Plug 'roman/golden-ratio'

" Never have to :set paste
Plug 'ConradIrwin/vim-bracketed-paste'

" Git and Github stuff
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'

" ys and cs for adding / changing surrounds
Plug 'tpope/vim-surround'

" gc for commenting out stuff, toggleable
Plug 'tpope/vim-commentary'

" Delicious fuzzy file finding
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Because im too lazy to write my own status line
Plug 'rbong/vim-crystalline'

" Syntax highlighting via treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

if g:os == "Darwin"
    " Dash plugin for mac
    Plug 'rizzatti/dash.vim'
elseif g:os == "Linux"
    " Zeal plugin for linux
    Plug 'KabbAmine/zeavim.vim'
endif

" Elixir filetype + syntax highlighting
Plug 'elixir-editors/vim-elixir', { 'for': 'elixir' }

" Themes
Plug 'morhetz/gruvbox'
call plug#end()

if plug_install
    PlugInstall --sync
endif
unlet plug_install
unlet plugged_data_path
" }}}

" Gruvbox {{{
let g:gruvbox_contrast_dark = "hard"
let g:gruvbox_invert_tabline = 1
let g:gruvbox_sign_column = 'bg0'

colorscheme gruvbox
" }}}

" ALE {{{
let g:ale_echo_msg_format = '[%linter%] %s [%severity%:%code%]'
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['autopep8', 'black', 'isort'],
\   'javascript': ['eslint'],
\   'vue': ['eslint'],
\   'dhall': ['dhall-format'],
\   'go': ['gofmt'],
\   'rust': ['rustfmt']
\}

let g:ale_python_autopep8_options = '--aggressive --ignore E501,E402'
let g:ale_python_flake8_options = '--max-complexity 10'
let g:dhall_options = '--ascii'
let g:ale_fix_on_save = 1

nmap <silent> <C-k> <Plug>(ale_previou_wrap)zz
nmap <silent> <C-j> <Plug>(ale_next_wrap)zz
" }}}

" FZF {{{
command! -bang -nargs=? GFilesCwd
  \ call fzf#vim#gitfiles(<q-args>, fzf#vim#with_preview(<q-args> == '?' ? { 'dir': getcwd(), 'placeholder': '' } : { 'dir': getcwd() }), <bang>0)

nnoremap <Leader>ft :Tags!<CR>
nnoremap <Leader>fb :Buffers<CR>
nnoremap <Leader>fr :Rg<CR>
nnoremap <Leader>fm :Marks<CR>
nnoremap <Leader>ff :Files<CR>
nnoremap <Leader>fg :GFilesCwd<CR>

let g:fzf_preview_window = ''
let g:fzf_layout = {
\   'window': {
\       'width': 0.1,
\       'height': 0.4,
\       'xoffset': 0.2,
\       'yoffset': 0.1,
\       'border': 'rounded'
\   }
\}
" }}}

" Crystalline {{{
function! StatusLine(current, width)
  return (a:current ? crystalline#mode() . '%#Crystalline#' : '%#CrystallineInactive#')
        \ . ' %f%h%w%m%r '
        \ . (a:current ? '%#CrystallineFill# %{fugitive#head()} ' : '')
        \ . '%=' . (a:current ? '%#Crystalline# %{&paste?"PASTE ":""}%{&spell?"SPELL ":""}' . crystalline#mode_color() : '')
        \ . (a:width > 80 ? ' %{&ft}[%{&enc}][%{&ffs}] %l/%L %c%V %P ' : ' ')
endfunction

function! TabLine()
  let l:vimlabel = has("nvim") ?  " NVIM " : " VIM "
  return crystalline#bufferline(2, len(l:vimlabel), 1) . '%=%#CrystallineTab# ' . l:vimlabel
endfunction

let g:crystalline_statusline_fn = 'StatusLine'
let g:crystalline_tabline_fn = 'TabLine'
let g:crystalline_theme = 'gruvbox'
" }}}

" Signify {{{
let g:signify_vcs_list = ['git']
" }}}

" Jedi {{{
let g:jedi#completions_enabled = 0
let g:jedi#use_splits_not_buffers = "right"
let g:jedi#show_call_signatures = 0
" }}}

" Treesitter {{{
lua <<EOF
  require'nvim-treesitter.configs'.setup {
    ensure_installed = "maintained",
    highlight = {
      enable = true,
    },
  }
EOF
" }}}
" }}}
