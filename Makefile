SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

all: kitty emacs tmux neovim zsh git starship general
.PHONY: all

kitty:
> stow -t /home/imran kitty
.PHONY: kitty

clean_kitty:
> stow -t /home/imran -D kitty
.PHONY: clean_kitty

emacs:
> stow -t /home/imran emacs
.PHONY: emacs

clean_emacs:
> stow -t /home/imran -D emacs
.PHONY: emacs

uptime_r: tmux/uptime_r/src/main.rs tmux/uptime_r/Cargo.toml
> cd tmux/uptime_r
> cargo install --path .

clean_uptime_r:
> cargo uninstall uptime_r
.PHONY: clean_uptime_r

tmux: uptime_r
> stow -t /home/imran tmux
.PHONY: tmux

clean_tmux:
> stow -t /home/imran -D tmux
.PHONY: clean_tmux

neovim:
> stow -t /home/imran neovim
.PHONY: neovim

clean_neovim:
> stow -t /home/imran -D neovim
.PHONY: clean_neovim

zsh:
> stow -t /home/imran zsh
.PHONY: zsh

clean_zsh:
> stow -t /home/imran -D zsh
.PHONY: clean_zsh

git:
> stow -t /home/imran git
.PHONY: git

clean_git:
> stow -t /home/imran -D git
.PHONY: clean_git

starship:
> stow -t /home/imran starship
.PHONY: starship

clean_starship:
> stow -t /home/imran -D starship
.PHONY: clean_starship

general:
> stow -t /home/imran general
.PHONY: general

clean_general:
> stow -t /home/imran -D general
.PHONY: clean_general

clean: clean_kitty clean_emacs clean_uptime_r clean_neovim clean_tmux clean_zsh clean_git clean_starship clean_general
.PHONY: clean
